include("hydro-data.jl")

mutable struct HydroDriver
    geometry::Geometry
    γ::Float64
    data::HydroData
    data_old::HydroData
    dt::Float64
    σ_artificial_viscosity::Float64
    dt_factor::Float64
    dt_density_ε::Float64
    dt_courant_factor::Float64
end

# constructor
function HydroDriver(;
    geometry::Geometry,
    γ::Float64,
    initial_position::Vector{Float64},
    initial_velocity::Vector{Float64},
    initial_pressure::Vector{Float64},
    initial_density::Vector{Float64},
    σ_artificial_viscosity::Float64,
    dt_initial_max::Float64,
    dt_factor::Float64,
    dt_density_ε::Float64,
    dt_courant_factor::Float64,
)::HydroDriver
    data = HydroData(;
        geometry = geometry,
        position = initial_position,
        velocity = initial_velocity,
        pressure = initial_pressure,
        density = initial_density,
    )
    data_old = deepcopy(data)

    hydro_driver = HydroDriver(
        geometry,
        γ,
        data,
        data_old,
        dt_initial_max / dt_factor,
        σ_artificial_viscosity,
        dt_factor,
        dt_density_ε,
        dt_courant_factor,
    )

    calculate_artificial_viscosity!(hydro_driver.data, hydro_driver.σ_artificial_viscosity)

    hydro_driver
end

dt_courant(hydro_driver::HydroDriver)::Float64 =
    hydro_driver.dt_courant_factor * minimum(
        diff(hydro_driver.data.position) .* (
            hydro_driver.γ * hydro_driver.data.pressure .* hydro_driver.data.volume ./
            hydro_driver.data.mass +
            abs.(hydro_driver.data.velocity[2:end]) +
            abs.(hydro_driver.data.velocity[1:end-1])
        ),
    )

dt_density(hydro_driver::HydroDriver)::Float64 =
    0.5 *
    hydro_driver.dt *
    hydro_driver.dt_density_ε *
    minimum(
        abs.(
            (hydro_driver.data.volume + hydro_driver.data_old.volume) ./
            (hydro_driver.data.volume - hydro_driver.data_old.volume),
        ),
    )

function solve_for_pressure!(hydro_driver::HydroDriver)::HydroDriver
    hydro_driver.data.pressure =
        (
            hydro_driver.data_old.volume ./ (hydro_driver.γ - 1.0) .*
            hydro_driver.data_old.pressure -
            (hydro_driver.data.volume - hydro_driver.data_old.volume) ./ 2.0 .* (
                hydro_driver.data_old.pressure +
                hydro_driver.data.artificial_viscosity +
                hydro_driver.data_old.artificial_viscosity
            )
        ) ./ (
            hydro_driver.data.volume ./ (hydro_driver.γ - 1.0) +
            (hydro_driver.data.volume - hydro_driver.data_old.volume) ./ 2.0
        )
    hydro_driver
end

function step!(hydro_driver::HydroDriver)::HydroDriver

    # calculating this step's dt
    hydro_driver.dt = min(
        hydro_driver.dt * hydro_driver.dt_factor,
        dt_courant(hydro_driver),
        # dt_density(hydro_driver),
    )

    # swapping the new and old HydroData members by reference
    hydro_driver.data_old, hydro_driver.data = hydro_driver.data, hydro_driver.data_old

    # stepping the partitions' positions with half step velocity
    hydro_driver.data.velocity += hydro_driver.data.acceleration * hydro_driver.dt / 2.0
    hydro_driver.data.position += hydro_driver.data.velocity * hydro_driver.dt

    # calculating the new volumes and artificial viscosities
    calculate_volumes!(hydro_driver.data)
    calculate_artificial_viscosity!(hydro_driver.data, hydro_driver.σ_artificial_viscosity)

    # calculating new pressures
    solve_for_pressure!(hydro_driver)

    # stepping the partitions' velocity a half step using the new accelerations
    calculate_acceleration!(hydro_driver.data)
    solve_for_pressure!(hydro_driver)

    # stepping the partitions' velocity a half step using the new accelerations
    calculate_acceleration!(hydro_driver.data)
    hydro_driver.data.velocity += hydro_driver.data.acceleration * hydro_driver.dt / 2.0

    hydro_driver
end
