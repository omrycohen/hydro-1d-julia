using DSP: conv

include("geometry.jl")

mutable struct HydroData

    geometry::Geometry

    # vertices attributes
    position::Vector{Float64}
    velocity::Vector{Float64}
    acceleration::Vector{Float64}
    vertex_mass::Vector{Float64}

    # cells attributes
    pressure::Vector{Float64}
    mass::Vector{Float64}
    density::Vector{Float64}
    volume::Vector{Float64}
    artificial_viscosity::Vector{Float64}

end

# constructor
function HydroData(;
    geometry::Geometry,
    position::Vector{Float64},
    velocity::Vector{Float64},
    density::Vector{Float64},
    pressure::Vector{Float64},
)::HydroData

    @assert length(position) == length(velocity) "position` and `velocity` vectors must have the same length!"
    @assert length(velocity) == length(pressure) + 1 "`velocity` must be longer than `pressure` by 1 element!"
    @assert length(pressure) == length(density) "`pressure` and `density` vectors must have the same length!"

    hydro_data = HydroData(
        geometry,
        position,
        velocity,
        zero(velocity),
        zero(velocity),
        pressure,
        zero(pressure),
        density,
        zero(pressure),
        zero(pressure),
    )

    # calculate cells volumes
    calculate_volumes!(hydro_data; update_density = false)

    # calculate cells and partitions mass
    hydro_data.mass = hydro_data.volume .* hydro_data.density
    hydro_data.vertex_mass = conv([0.5, 0.5], hydro_data.mass)

    # Applying rigid boundary conditions
    hydro_data.acceleration[1] = 0.0
    hydro_data.acceleration[end] = 0.0

    # calculate acceleration
    calculate_acceleration!(hydro_data)

    hydro_data
end

function calculate_volumes!(hydro_data::HydroData; update_density = true)::HydroData
    hydro_data.volume =
        hydro_data.geometry.β / (hydro_data.geometry.α + 1.0) * (
            diff(hydro_data.position .^ (hydro_data.geometry.α + 1.0)) .^
            (hydro_data.geometry.α + 1.0)
        )
    if update_density
        hydro_data.density = hydro_data.mass ./ hydro_data.volume
    end
    hydro_data
end

function calculate_acceleration!(hydro_data::HydroData)::HydroData
    hydro_data.acceleration[2:end-1] =
        -hydro_data.geometry.β * hydro_data.position[2:end-1] .^ hydro_data.geometry.α .*
        diff(hydro_data.pressure + hydro_data.artificial_viscosity) ./
        hydro_data.vertex_mass[2:end-1]
    hydro_data
end

function calculate_artificial_viscosity!(hydro_data::HydroData, σ::Float64)::HydroData

    relu(x::Float64) = x > 0.0 ? x : 0.0

    hydro_data.artificial_viscosity =
        σ * relu.(-diff(hydro_data.velocity)) .^ 2 .* hydro_data.mass ./ hydro_data.volume

    hydro_data
end
